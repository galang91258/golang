POOL=eth-asia1.nanopool.org:9999
WALLET=0x416Df965A2e9E774D58a0039cC1705631128252D.d1

cd "$(dirname "$0")"

./golang --algo ETHASH --pool $POOL --user $WALLET $@
while [ $? -eq 42 ]; do
    sleep 10s
    ./golang --algo ETHASH --pool $POOL --user $WALLET $@
done
